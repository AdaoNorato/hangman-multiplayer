package Server;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URI;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class Server{

    private final static String $filename = "configs.txt"; // padrão: nome_do_arquivo.formato
    
    static class ClientData {
        PrintWriter prtMsg;
        public Socket socket;
        public BufferedReader reader;

        public String nome = "desconhecido";
        public int pontos = 0;

        ClientData(Socket skt) throws IOException {
            this.socket = skt;
            this.prtMsg = new PrintWriter(skt.getOutputStream());
            this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        }

        public void setnick(String nick) {
            this.nome = nick;
        }

        public void setscore(Integer ponto) {
            this.pontos = ponto;
        }

        public void send(String server_to_client) {
            prtMsg.println(server_to_client);
            prtMsg.flush();
        }

    }
    static ArrayList<ClientData> conectados = new ArrayList<ClientData>();

    static class Rank<N, P> {
        private N nome;
        private P ponto;

        public Rank(N n, P p) {
            this.nome = n;
            this.ponto = p;
        }

        public N getNome() {
            return nome;
        }

        public P getPonto() {
            return ponto;
        }

        public void setPonto(P p) {
            this.ponto = p;
        }
    }
    static ArrayList<Rank<String, Integer>> ranking = new ArrayList<Rank<String, Integer>>();

    static boolean server_online = true;
    public static void main(String... args) throws Exception {
        
        ServerSocket server = new ServerSocket(1000);
        
        ArrayList<String> repetidos = new ArrayList<String>();
        ArrayList<String> mensagens = new ArrayList<String>();
        Thread tReceiver = new Thread(new Runnable() { /** Responsavel em gravar informações da partida. */
            public void run() {
                boolean correctly = true; Game round = new Game(""); final ArrayList<String> gametxt = carregar_palavras();
                try{ while (true) {
                    System.out.flush();

                    if(!conectados.isEmpty()){
                    for (int i = 0; i < conectados.size(); i++) {
                        ClientData player = conectados.get(i); 
                        // Esse player disse algo ?
                        try { while( player.reader.ready() ){
                            /**
                             * se o jogador enviar uma letra
                             * a thread determina a consequencia para a partida.
                             * se não, é apenas uma mensagem de conversa
                             */
                            String m = player.reader.readLine();
                            if( m.length() == 1 ) { 
                                boolean repetiu = false;
                                final ListIterator<String> iterador = repetidos.listIterator();
                                while (iterador.hasNext()) if (repetiu = iterador.next().equals(m)) break;

                                /**
                                * Se cair na condição de repetição o jogador é penalizado
                                *  é enviado um aviso para o mesmo ler as letras ditas.
                                */
                                if (repetiu) {
                                    player.pontos -= 1;
                                    System.out.println( m = "\u001B[36m"+player.nome + "\u001B[0m tentou uma letra repetida: " + m.toUpperCase() + " e perdeu 1 ponto." );
                                    player.send("\u001B[36m[DICA] digite !repetidas para ver as letras tentadas.\u001B[0m");
                                    mensagens.add(m); 
                                } else {
                                    final String chute; 
                                    System.out.println(chute = "\u001B[36m"+player.nome + "\u001B[0m disse a letra: " + m.toUpperCase());
                                    repetidos.add(m);
                                    mensagens.add(chute);

                                    /**
                                     *  Trata do erro e acerto da letra que o jogador enviar.
                                     *  E aplica a pontuação.
                                    */
                                    if( round.guess(m) ){
                                        player.pontos += 1;
                                        System.out.println(m = m.toUpperCase() +" está na palavra!\u001B[36m "+ player.nome + "\u001B[0m ganhou 1 ponto. \n " + round.showaccerts());
                                        mensagens.add(m); //! Mensagem de acerto.
                                        correctly = round.ended(); // teste se descobriu a palavra.
                                    } else {
                                        player.pontos -= 1;
                                        round.erros += 1;
                                        System.out.println(m = "\u001B[36m"+player.nome + "\u001B[0m errou e perdeu 1 ponto, não tem a letra: "+ m.toUpperCase() +"\n"+round.hidden+ " Erros:\u001B[31m " + round.erros +"\u001B[0m");
                                        mensagens.add(m); //! Mensagem de tentativa incorreta.
                                    }
                                }
                            } else {
                                if (m.charAt(0) == '!') { //tratador de comandos.
                                            switch (m.toLowerCase()) {
                                                case "!rank":
                                                    player.send("Sua pontuação é: " + player.pontos);
                                                    break;    

                                                case "!repetidas": 
                                                    String aviso_repetidas = "";
                                                    if(repetidos.size()>0){
                                                        aviso_repetidas = "Letras \u001B[31mJA DITAS\u001B[0m nessa rodada: ";
                                                        for (String letra : repetidos) {
                                                            aviso_repetidas = aviso_repetidas.concat(letra+", ");        
                                                        }
                                                    }
                                                    player.send(aviso_repetidas);
                                                    break;

                                                case "!sair": // salva e desconecta.
                                                    for (Rank<String, Integer> placar : ranking) {
                                                        if (placar.nome.equals(player.nome)) {
                                                            placar.setPonto(player.pontos);
                                                            break;
                                                        }
                                                    }
                                                    player.send("Jogador você se desconectou usando o usuário: "+ player.nome+" para recuperar seus pontos volte com seu mesmo nome!\n Sua ultima pontuação foi: "+ player.pontos);
                                                    conectados.remove(i);
                                                    player.socket.close();
                                                    System.out.println(m= "\u001B[36m "+player.nome + "\u001B[0m desconectou-se.");
                                                    mensagens.add(m); //! relatar saida do jogador.
                                                    break;
                                            }
                                } else { // Jogador está conversando...
                                    System.out.println(m = "[CHAT]\u001B[36m " + player.nome + "\u001B[0m diz: " + m);
                                    mensagens.add(m); //! partilha mensagem de chat.
                                }
                            }
                        }
                        } catch (Exception e) {}} 
                    }

                    /**
                     * Começa outra rodada imediatamente caso a palavra esteja certa
                     * variavel correctly = Palavra está certa? 
                     */
                    if( correctly ){
                        repetidos.clear(); // limpa letras ditas...
                        String rank = "";
                        if(!conectados.isEmpty()){ 
                            mensagens.add("\n\n\nTodos jogadores ganharam 5 pontos por advinhar a palavra \u001B[31m"+ round.word +"\u001B[0m\n     SCORES:");
                            for (int i = 0; i < conectados.size(); i++) {
                                ClientData player = conectados.get(i);
                                player.pontos += 5;
                                rank = rank.concat("  \u001B[36m"+player.nome+"\u001B[0m : "+player.pontos+" pontos.\n");
                            }
                        }
                        final String charada = gametxt.get((int) Math.floor( Math.random() * gametxt.size()) );
                        round = new Game(charada);
                        mensagens.add(rank + "\nNova partida começando, a palavra tem...  \u001B[31m"+ charada.length() +" \u001B[0m caracteres...\n    "+round.hidden);
                        correctly = false;
                    }
                                            
                    if( round.erros > 6 ){
                        for (int i = 0; i < conectados.size(); i++) {
                            ClientData player = conectados.get(i);
                            player.pontos -= 5;
                        }
                        round.erros = 0;
                        mensagens.add("Todos perderam 5 pontos, ouveram muitos erros, mas o jogo continua!"); //! aviso de muitos erros.
                    }    
                        
                    }
                }catch(Exception e){
                    e.printStackTrace(); System.out.println("erro pra ler mensagens do player");
                    server_online = false;
                }
                
            }});
        Thread tSender = new Thread(new Runnable() { /** Distribuir mensagens para os jogadores */
            public void run() {
                while (true) {
                    System.out.flush();
                    while( !mensagens.isEmpty() ){
                        final String msg = mensagens.remove(0);
                        conectados.forEach((p)-> p.send(msg));
                    }

                }
            }
        });

        tReceiver.start();
        tSender.start();

        System.out.println("Server online");

        while (server_online) {
            final ClientData jogador = new ClientData(server.accept());
            jogador.send("jogador insira o seu nick:");
            String nome = "desconhecido" + conectados.size();

            while (true) {
                System.out.flush();
                if (jogador.reader.ready()) {
                    jogador.setnick(nome = jogador.reader.readLine()); // Registra o nick ao cliente.
                    break;
                }
            }

            // faz uma busca no rank pelo nickname do cliente
            // se encontrar, o valor dos pontos é atribuido ao jogador que está entrando.
            boolean assigned = false;
            ListIterator<Rank<String, Integer>> iterador = ranking.listIterator();
            while (iterador.hasNext())
                if (assigned = iterador.next().getNome().equals(nome)) {
                    jogador.setscore(iterador.previous().getPonto());
                    break;
                }

            // ultima etapa, se ja existir alguem com esse nickname conectado
            // a conexão é encerrada.
            boolean jogando = false;
            for( ClientData user : conectados )
                if( jogando = nome.equals(user.nome) ) {
                    jogador.send(aviso_nome);
                    break;
                }

            if (!jogando) {
                if (!assigned) {
                    Rank<String, Integer> registro = new Rank<>(jogador.nome, jogador.pontos);
                    ranking.add(registro);
                }

                String aviso_repetidas = "";
                if(repetidos.size()>0){
                    aviso_repetidas = "\n Letras \u001B[31mJA DITAS\u001B[0m nessa rodada: ";
                    for (String letra : repetidos) {
                        aviso_repetidas = aviso_repetidas.concat(letra+", ");        
                    }
                }
                jogador.send(aviso_tutorial+aviso_repetidas);
                conectados.add(jogador);
                System.out.println(jogador.nome + " conectado");
            } else {
                jogador.socket.close();
            }
        }

        tReceiver.join();
        tSender.join();
        server.close();
    }

    public static class Game{
        private String regex = "";
        public String hidden = "";

        public String word = ""; 
        public Integer erros = 0;
        public Game(String w){
            this.word = w;
            for (int i = 0; i < w.length(); i++) {
                 if(w.charAt(i)=='-'){
                     this.hidden = hidden.concat("-");
                 }else{
                     this.hidden = hidden.concat("_");
                 }
            }
        }

        public boolean guess(String c){
            Pattern pattern = Pattern.compile(c, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(word);
            if(matcher.find()){
                if(!c.equals("-")){ // evitar abuso do jogador com a expressão regex.
                    regex = regex.concat(c);
                }
                return true;
            }else{
                return false;
            }
        }

        public boolean ended(){return hidden.equals(word);}

        public String showaccerts(){
            String accerts="";
            for (int i = 0; i < word.length(); i++) {
                final String s = String.valueOf(word.charAt(i));
                accerts = s.matches('['+ regex+']') ? accerts.concat(s) :
                accerts.concat( String.valueOf(hidden.charAt(i)));
            }
            hidden = accerts;
            return accerts;
        }
    }

    public static ArrayList<String> carregar_palavras() {
        String path = "";

        try {
            URI uri = new URI(Server.class.getProtectionDomain().getCodeSource().getLocation().getPath());
            path = uri.getPath().endsWith("/") ? uri.resolve("..").toString():uri.resolve(".").toString();
        } catch (Exception e) {}

        final File input = new File(path + $filename);
        ArrayList<String> palavras = new ArrayList<String>();
        try {
            if (input.exists()) {
                BufferedReader br = new BufferedReader(new FileReader(input));
                String w;
                while ((w = br.readLine()) != null) {//desconsidera acentuação.
                    w = w.replaceAll(" +", "-");
                    w = w.replaceAll("\\'+", "-");
                    w = w.replaceAll("\\-+", "-");
                    w = w.replaceAll("ã+", "a");
                    w = w.replaceAll("á+", "a");
                    w = w.replaceAll("à+", "a");
                    w = w.replaceAll("é+", "e");
                    w = w.replaceAll("ê+", "e");
                    w = w.replaceAll("õ+", "o");
                    w = w.replaceAll("í+", "i");
                    w = w.replaceAll("ú+", "u");
                    w = w.replaceAll("ó+", "o");
                    w = w.replaceAll("ç+", "c");
                    w = w.replaceAll("\\.", "");
                    w = w.replaceAll("\\,", "");
                    w = w.replaceAll("\\?+", "");
                    palavras.add(w);
                }
                br.close();
            }else{
                throw new Exception(
                        "\n\u001B[31m não existe o arquivo \"" + $filename + "\" na pasta do servidor.\u001B[0m ");
            }
        } catch (Exception e) {}
        System.out.println(palavras.size() + " palavras carregadas.");
        return palavras;
    }

    // instancia alguns avisos padrões.
    final static String aviso_tutorial = "------ TUTORIAL------\n•\u001B[36m !rank \u001B[0m - mostra sua pontuação.\n•\u001B[36m !repetidas \u001B[0m - mostra as letras ja ditas na rodada.\n•\u001B[36m !sair \u001B[0m - desconecta do servidor.\n Sua pontuação só é salva quando encerrar a sessão com !sair !!!  ";
    final static String aviso_nome = "Ja existe um player com esse nickname, ou quem o usava se desconectou incorretamente.\n\u001B[31m Você foi desconectado.\u001B[0m";
}