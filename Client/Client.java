package Client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
public class Client {

    private static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) throws IOException, InterruptedException{
        Socket endpoint = new Socket("localhost",1000);
        PrintWriter bytes = new PrintWriter(endpoint.getOutputStream());
        
        String msg = "";

        Thread serverMessages = new Thread(new Runnable(){
            public void run(){
                try{
                    BufferedReader serverReader = new BufferedReader(new InputStreamReader(endpoint.getInputStream()));
                    String svMsg;
                    while(true){
                        System.out.flush();
                        while ((svMsg = serverReader.readLine()) != null){
                            System.out.println(svMsg);
                        }
                    }
                }catch(Exception e){}
            }
        });
        serverMessages.start();

        while(!msg.equals("!sair")){
            msg = sc.nextLine();
            try{

                bytes.println(msg);
                bytes.flush();
            }catch(Exception e){
                e.printStackTrace();
            }
        }

        endpoint.close();
        serverMessages.join();
        bytes.close();
    }
}
