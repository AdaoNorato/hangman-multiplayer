# hangman multiplayer 1.0.0

projeto 2 da materia de sistema operacionais do fim de ano.
## Projeto solo: 
dev: Adão Norato Claro Junior.
<br>

### Executar primeiro o servidor, e em seguida os clientes podem entrar.
```
INSTRUÇÕES
(modo 1) pode apresentar bugs visuais em acentuação e simbolos, Nesse mesmo diretório aberto digite:

para server:    java -jar Server.jar       
para client:    java -jar Client.jar    

(modo 2) instrução: abra os diretórios Client e Server, e use o seguinte comando

para server:    java Server.java       
para client:    java Client.java       
```
Aviso: se um cliente sair durante o registro de nome, o servidor não vai aceitar mais que ninguem entre, sera necessário reiniciar o servidor.
<br><br>
## PICS:
### <br> Comandos
![in-game](pics/conectar.png)
<br>
<br>
<br>
### <br> Demonstração comandos :
![comandos](pics/comandos.png)
<br>
<br>
### <br> Salvar pontuação:
![sair](pics/sair.png)
<br>
![reconectar](pics/gif.gif)
<br>
<br>
### <br> Pontuação
![reconectar](pics/pontuacao.png)

